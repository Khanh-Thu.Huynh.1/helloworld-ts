FROM node:slim as builder

WORKDIR /app
#3. Copy the package.json and package-lock.json files to the /app directory
COPY package*.json ./
#3. Install the dependencies
RUN npm install
#4. Copy the rest of the files to the /app directory
COPY . .
#4. Build the app (run script called build in package.json file)
RUN npm run build
#5. Create a new image
FROM node:alpine

WORKDIR /app
#6. Copy the dist folder from the builder image to the /app directory
COPY --from=builder /app/dist ./dist

COPY package*.json ./
#7. Install only production dependencies
RUN npm install --omit=dev

EXPOSE 3000

CMD [ "node", "dist/index.js" ]


